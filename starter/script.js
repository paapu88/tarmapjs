'use strict';
if (navigator.geolocation)
    navigator.geolocation.getCurrentPosition(function (position) {
        const latitude = position.coords.latitude
        const longitude = position.coords.longitude
        var mymap = L.map('mapid').setView([latitude, longitude], 13);


        L.tileLayer(
            'http://b.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png'
            //'https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png'
            //'https://{s}.tile.waymarkedtrails.org/cycling/{z}/{x}/{y}.png'
            , {
                attribution:
                    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            }).addTo(mymap);


        L.marker([latitude, longitude]).addTo(mymap)
            .bindPopup("<b>Hello world!</b><br />I am a popup.").openPopup();



        var popup = L.popup();

        function onMapClick(e) {
            popup
                .setLatLng(e.latlng)
                .setContent("You clicked the map at " + e.latlng.toString())
                .openOn(mymap);
        }

        mymap.on('click', onMapClick);
        var myStyle = {
            "color": "red",
            "weight": 5,
            "opacity": 0.65
        };
        var stater = L.geoPackageFeatureLayer([], {
            geoPackageUrl: '/home/hu-mka/git/Omat/tarMapJs/uusimaa.gpkg',
            style: myStyle
        }).addTo(mymap);


    }, function () { alert('problem with position') })

/*
if (navigator.geolocation)
    navigator.geolocation.getCurrentPosition(function (position) {
        const latitude = position.coords.latitude
        const longitude = position.coords.longitude
        const mapid = L.map('mapid').setView([latitude, longitude], 13);

        L.tileLayer(
            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a>',
            maxZoom: 6
        }).addTo(map)
        /*
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1
        }).addTo(mymap);

        L.marker([latitude, longitude]).addTo(mymap)
            .bindPopup("<b>Hello world!</b><br />I am a popup.").openPopup()
    }, function () { alert('problem with position') })
*/